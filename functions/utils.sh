function installed {
	command -v $1 >/dev/null 2>&1 || hash $1 2>/dev/null || type $1 >/dev/null 2>&1 || return 1
}

function ordinal {
	local string="$1"
	local exp1='(.*)\\e\[[0-9\;]+m(.*)' # Select AROUND our "style" codes
	local exp2='(.*)\\u00a0(.*)' # REMOVE any non-breaking-spaces
	while [[ "$string" =~ $exp1 ]]; do
		string="${BASH_REMATCH[1]}${BASH_REMATCH[2]}"
	done
	while [[ "$string" =~ $exp2 ]]; do
		string="${BASH_REMATCH[1]}_${BASH_REMATCH[2]}"
	done
	echo ${#string}
}

function round {
	local val=$(arg 1 $1)
	local scale=$(arg 2 $2)
	printf %.0f $(bc <<< "scale=$scale;(((10^$scale)*($val))+0.5)/(10^$scale)")
}

# Leave now for legacy purposes.
function arg {
	local default=$1
	local value=$2
	if [[ $value == "" ]]; then
		value=$default
	fi
	printf $value
}
