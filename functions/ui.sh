source ~/utils.sh

declare -A COLOURS

COLOURS[none]=0
COLOURS[black]=30
COLOURS[red]=31
COLOURS[green]=32
COLOURS[yellow]=33
COLOURS[blue]=34
COLOURS[purple]=35
COLOURS[cyan]=36
COLOURS[grey]=37
COLOURS[gray]=37 # ugh
COLOURS[slate]=90
COLOURS[orange]=91
COLOURS[light-green]=92
COLOURS[light-yellow]=93
COLOURS[light-blue]=94
COLOURS[light-purple]=95
COLOURS[light-cyan]=96
COLOURS[white]=97


declare -A ARGS

REGULAR="-r"
BOLD="-b"
UNDERLINE="-u"
COLOUR="-c"
BACKGROUND="-bg"

ARGS[$REGULAR]=0
ARGS[$BOLD]=1
ARGS[$UNDERLINE]=4
ARGS[$COLOUR]=0
ARGS[$BACKGROUND]=10

PAD=0
PAD_SIZE=1
TAB=0
TAB_SIZE=4
NBSP="\u00a0"
MAX_WIDTH=80

function multiply {
	local text=$1
	local quantity=$2
	local output=""
	local count=0
	while [ $count -lt $quantity ]; do
		output=$output$text
		((count++))
	done
	echo $output
}

function calc-width {
	local offset=$(arg 0 $1)
	local WIDTH=$(tput cols)
	if [ $WIDTH -lt $MAX_WIDTH ]; then
		MAX_WIDTH=$WIDTH
	fi
	local tabs_offset=$((TAB * TAB_SIZE))
	local padding=$((PAD * 2 * PAD_SIZE))
	echo $((MAX_WIDTH - offset - tabs_offset - padding))
}

function clear-line {
	tput el
}

function style {
	local mode=none
	local code=""
	for arg in $@; do
		if [[ $mode == none ]]; then
			if [[ $arg == $COLOUR ]] || [[ $arg == $BACKGROUND ]]; then
				mode=$arg
			else
				if [[ ! $code == "" ]]; then
					code=$code";"
				fi
				code=$code${ARGS[$arg]}
			fi
		else
			local color_code=${COLOURS[$arg]}
			local color_offset=${ARGS[$mode]} # We +10 to the color_code if the option passed is -bg (background)
			local color=$((color_code + color_offset))
			if [[ ! $code == "" ]]; then
				code=$code";"
			fi
			code=$code$color
			mode=none
		fi
	done
	if [[ $code == "" ]]; then
		code=0
	fi
	echo "\e["$code"m"
}

CLEAR=$(style)

function text {
	local message=$1
	shift
	local start=`style $@`
	echo "$start$message$CLEAR"
}


# The inserts empty spaces (multiply + space)
function fill {
	local char=$1 && shift
	local width=$1 && shift
	echo $(multiply $char $width)
}

function gap {
	local width=$1 && shift
	echo $(fill "$NBSP" $width)
}

function span {
	local message=$1 && shift
	local width=$(arg $(calc-width ${#message}) $1)
	echo $message$(gap $width)
}


function icon {
	local symbol=$(arg "?" $1) && shift
	local fg=$(arg black $1) && shift
	local bg=$(arg white $1) && shift
	echo $(text "$NBSP$symbol$NBSP" -b -bg $bg -c $fg $@)
}

function label {
	local msg=$1 && shift
	local bg=$(arg black $1) && shift
	local fg=$(arg white $1) && shift
	echo $(text "$NBSP$msg$NBSP" -b -bg $bg -c $fg $@)
}

function icon-label {
	local symbol=$1
	local message=$2
	local colour=$3
	echo $(icon "$symbol" $colour)$(label "$message" $colour)
}




function none {
	echo ""
}

function numbered {
	echo "$(tab_gap 2)$(text $1 -b).$NBSP"
}

function list {
	local handler=$1
	shift
	local output=""
	local count=0
	for line in "$@"; do
		((count++))
		output="$output$($handler $count)$line\n"
	done
	echo $output
}

function multiline {
	echo $(list none "$@")
}

function line-gap {
	echo $(gap $(calc-width))
}

function line-rule {
	echo $(aside "$(fill "$(arg · $1)" $(calc-width))")
}

function line-break {
	echo $(multiline "$(line-gap)" "$(line-rule $1)")
}


function paragraph {
	local line_length=$(calc-width $((TAB_SIZE * 2)))
	local output=""
	local count=0
	for word in $@; do
		local size=${#word}
		local next=$((count + 1 + size))
		if [ $next -le $line_length ]; then
			if [ $count -gt 0 ]; then
				output=$output$NBSP
			else
				output=$output$(tab_gap)
			fi
			output=$output$word
			count=$next
		else
			output=$output"\n"$(tab_gap)$word
			count=$size
		fi
	done
	echo-block "\n"$output"\n"
}


function header {
	local message=$1
	echo-line $(line-rule •)
	pad
	echo-block $(text "$message" -b)
	unpad
	echo-line $(line-rule •)
}

function aside {
	local message=$1
	shift
	echo $(text "$message" -c slate $@)
}

function hint {
	local message=$1
	shift
	echo $(icon "❤" white red)$(label "TIP" red white) $(text "$message" $@)
}

function entry {
	local message=$1
	shift
	echo $(icon-label "✔" "INPUT" cyan) $(text "$message" $@)
}

function info {
	local message=$1
	shift
	echo $(icon-label "i" "INFO" light-blue) $(text "$message" $@)
}

function warn {
	local message=$1
	shift
	echo $(icon-label "!" "WARNING" orange) $(text "$message" -c light-yellow $@)
}

function error {
	local message=$1
	shift
	echo $(icon-label "✘" "ERROR" red) $(text "$message" -c orange $@)
}

function ok {
	local message=$1
	shift
	echo $(icon-label "✔" "OK" green) $(text "$message" -c light-green $@)
}

function complete {
	local message=$1
	shift
	echo $(icon-label "⭐" "DONE" light-green) $(text "$message" $@)
}



function progress {
	local progress=$(arg 0 $1)
	local width=$(arg 60 $2)
	local fill=$(text "$(arg '▒' $3)" -c light-cyan)
	local bg=$(text "$(arg '░' $4)")

	local cols=$(round "($progress/100)*$width")
	local diff=$((width-$cols))

	local bar=$(multiply "$fill" $cols)$(multiply "$bg" $diff)

	echo "$bar"
}

function progress-ui {
	local symbol=$1
	local text=$2
	local per=$3
	local offset=$(arg 0 $4)
	local pre=$(icon-label "$symbol" "$text - $per%" blue)
	local size=$(ordinal "$pre")
	offset=$((offset + size))
	local width=$(calc-width $offset)
	echo $pre$(progress $per $width)
}





function pad {
	incr=$(arg 1 $1)
	PAD=$((PAD + incr))
}

function unpad {
	incr=$(arg 1 $1)
	PAD=$((PAD - incr))
}

function tab {
	incr=$(arg 1 $1)
	TAB=$((TAB + incr))
}

function untab {
	incr=$(arg 1 $1)
	TAB=$((TAB - incr))
}

function format_gap {
	local size=$1
	local factor=$(arg 1 $2)
	echo $(gap $((size * factor)))
}

function tab_gap {
	echo $(format_gap $TAB_SIZE $(arg 1 $1))
}

function tabs {
	echo $(tab_gap $TAB)
}

function pad_gap {
	echo $(format_gap $PAD_SIZE $(arg 1 $1))
}

function pads {
	echo $(pad_gap $PAD)
}





function render {
	echo -ne $@
}

function render-line {
	echo -e $@
}

function place {
	render $(tabs)$@
}

function place-line {
	place $@"\n"
}

function echo-line {
	echo $(pads)$@"\n"
}

# TODO: do we keep this or multiline?
function echo-lines {
	echo $(list none "$@")
}

function echo-block {
	render-line $@ | block
}





function stream-start {
	place $@ && tput sc
}

function stream {
	render $@ && tput el && tput rc
}

function stream-end {
	render $@ && tput el && render "\n"
}



# TODO: block, print, and view are all similar.
# we should abstract such that a handler/"callback" is used.
# everything is essentially the same... something to think aboot.
# THEY'RE ALL types of VIEWS. Or at least, we could do some sort of looping... read-loop... HMM!
function line-loop {
	echo "implement this, bitch."
}

function block {
	while read line; do
		echo-line $line
	done
}

function print {
	local padding=$(arg 0 $1)
	tab $padding
	while read line; do
		place-line $line
	done
	untab $padding
}

function view {
	local handler=$1
	shift
	local count=0
	while read data; do
		if ! $handler "$data" $count $@; then
			break
		fi
		((count++))
	done
}



# Shortcuts, baby.
function put {
	render-line $@ | print
}

function section-start {
	put $(line-gap)
	put $(header "$1")
}

function section-end {
	put $(line-break)
}




# inout is basic... shouldn't be used directly
function input {
	local message=$1
	shift
	local default=$1
	local options=""
	if [[ ! $default == "" ]]; then
		local lp=$(aside "(")
		local rp=$(aside ")")
		shift
		options=$(aside "$default" -b)
		for option in $@; do
			options=$options$(aside "|$option")
		done
		options="$lp$options$rp"
	fi
	message=$(icon-label "➤" "$message" orange)
	message="$message $options:$NBSP"

	read -p "$(place $message)" response
	if [[ $response == "" ]]; then
		response=$default
	fi
	echo $response
}


function input-and-confirm {
	local message=$1
	shift
	local options=$@
	while true; do
		local response=$(input "$message" $options)
		
		case $response in
			[yY]* )
				put $(entry "yes")
				return 0
				;;
			[nN]* )
				put $(entry "no")
				return 1
				;;
			* )
				put $(hint "$err")
				;;
		esac 
	done
}

function confirm {
	local message=$1
	local err=$2
	if [[ $err == "" ]]; then
		err="Please provide a yes or no answer...\n"
	fi
	while true; do
		local response=$(input "$message" yes no)

		# We should abstract this while loop and case stuff
		# I think it should be very possible....
		case $response in
			[yY]* )
				put $(entry "yes")
				return 0
				;;
			[nN]* )
				put $(entry "no")
				return 1
				;;
			* )
				put $(hint "$err")
				;;
		esac
	done
}






function spinner {
	local data=$1
	local count=$2
	local sprite=$3
	local frame=$((count%${#sprite}))
	local symbol=${sprite:$frame:1}
	stream $symbol$NBSP$(( ( 16 - count ) / 4))
}

function progress-bar {
	stream $(progress-ui 📥 'DOWNLOADING' $1 1)
}
