Dark Inkscape Theme v0.1 for Inkscape 0.92.4

Copyright (c) 2019, Simone Bottino <simo.bot87@gmail.com>

Original GIMP theme from Benoit Touchette, Copyright (c) 2015-2016
(please refer to gtk-2.0/gtkrc for the complete copyright notices)

###

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

###

HOW TO INSTALL ON WINDOWS

1) GO TO C:\Program Files\Inkscape\share\themes\MS-Windows
2) RENAME your gtk-2.0 folder to gtk-2.0-original
3) COPY the provided gtk-2.0 folder

4) GO TO C:\Program Files\Inkscape\share\templates
5) RENAME your default.svg to default.svg-original
6) COPY the provided default.svg
